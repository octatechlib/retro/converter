# Converter



## Getting started

Remember to install ROM tools first (e.g. Mac)
```bash
brew install rom-tools
```

Why do I need to convert anything?
- You will scan files later. Your repo has probably two files: bin and cue. Converter generates chd out of it. Remember to scan proper extensions. You can always put file names into list and make file .m3u out of what you want to scan.


## BIOS per each console

Read [here](https://www.psxdev.net/forum/viewtopic.php?t=56).

Notes:
- The SCPH-5003 is actually just a SCPH-1001 [MD5 CONFIRMED BY BAD_AD84).
- The SCPH-7003 is actually just a SCPH-5501 V3.0A [MD5 NOT CONFIRMED].
- The SCPH-9002 is actually just a SCPH-7002 [MD5 NOT CONFIRMED].

List of BIOS files you may need:

- [Download](http://psxdev.net/downloads/bios/psx/SCPH1000.BIN) SCPH-1000 [NTSC-J] dumped by N/A

- [Download](http://psxdev.net/downloads/bios/psx/SCPH1001.BIN) SCPH-1001 [NTSC-U/C] dumped by N/A

- [Download](http://psxdev.net/downloads/bios/psx/SCPH1002.BIN) SCPH-1002 [PAL] dumped by N/A

- [Download](http://psxdev.net/downloads/bios/psx/SCPH-3000.BIN) SCPH-3000 [NTSC-J] dumped by N/A

- [Download](http://psxdev.net/downloads/bios/psx/SCPH-3500.BIN) SCPH-3500 [NTSC-J] dumped by SCPH-1002

- [Download](http://psxdev.net/downloads/bios/psx/SCPH5000.BIN) SCPH-5000 [NTSC-U/C] `[BAD DUMP - DO NOT USE]`

- [Download](http://psxdev.net/downloads/bios/psx/SCPH-5501.BIN) SCPH-5501 [NTSC-U/C] dumped by Shadow

- [Download](http://psxdev.net/downloads/bios/psx/SCPH5500.BIN) SCPH-5500 [NTSC-J] dumped by N/A

- [Download](http://psxdev.net/downloads/bios/psx/SCPH5502.BIN) SCPH-5502 [PAL] dumped by Shadow

- [Download](http://psxdev.net/downloads/bios/psx/SCPH5552.BIN) SCPH-5552 [PAL] dumped by N/A

- [Download](http://psxdev.net/downloads/bios/psx/SCPH7000.BIN) SCPH-7000 [NTSC-J] dumped by N/A

- [Download](http://psxdev.net/downloads/bios/psx/SCPH7001.BIN) SCPH-7001 [NTSC-U/C] dumped by N/A

- [Download](http://psxdev.net/downloads/bios/psx/SCPH7002.BIN) SCPH-7002 [PAL] dumped by N/A

- [Download](http://psxdev.net/downloads/bios/psx/SCPH7003.BIN) SCPH-7003 [NTSC-J] dumped by N/A

- [Download](http://psxdev.net/downloads/bios/psx/SCPH7501.BIN) SCPH-7501 [NTSC] dumped by Tamaki310

- [Download](http://psxdev.net/downloads/bios/psx/SCPH-7502.BIN) SCPH-7502 [PAL] dumped by N/A

- [Download](http://psxdev.net/downloads/bios/psx/SCPH9002%287502%29.BIN) SCPH-9002 [PAL] dumped by N/A

- [Download](http://psxdev.net/downloads/bios/psx/SCPH-100.BIN) SCPH-100 [NTSC-J] dumped by SCPH-1002

- [Download](http://psxdev.net/downloads/bios/psx/SCPH101.BIN) SCPH-101 [NTSC-U/C] dumped by N/A

- [Download](http://psxdev.net/downloads/bios/psx/SCPH-102A.BIN) SCPH-102A [PAL] dumped by SCPH-1002

- [Download](http://psxdev.net/downloads/bios/psx/SCPH-102B.BIN) SCPH-102B [PAL] dumped by SCPH-1002

- [Download](http://psxdev.net/downloads/bios/psx/SCPH-102C.BIN) SCPH-102C [?] dumped by N/A


How to use them?
Go to your app, follow and browse:
```
RetroArch -> Settings -> Directory -> System/BIOS
```
Put files there. In my case I've created new dir under app content called `system` and put all BINs there.

## Troubleshooting

```bash
Error: firmware is missing. scph5501.bin
```

You did not follow instructions above.

## Authors and acknowledgment
Octatech

List of links copied from [PSXDEV](https://www.psxdev.net/forum/viewtopic.php?t=56). Instructions gathered from [WIKI](https://emulation.gametechwiki.com/index.php/Emulator_files). On how to install ROM tools and use chd files read [HERE](https://retrogamecorps.com/2023/02/06/the-ultimate-rom-file-compression-guide/).

## License
[![License](https://img.shields.io/badge/License-GPL_2.0-blue.svg)](https://opensource.org/license/gpl-2-0/)

## Project status
Scratch
